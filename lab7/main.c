#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>

#define honey_portion 50

int main(int argc, char **argv) //bee_count, hungry_time, eat_time, food_portion
{
    int bee_count = atoi(argv[1]);
    int hungry_time = atoi(argv[2]);
    int eat_time = atoi(argv[3]);
    int food_portion = atoi(argv[4]);
    int honey_count = 0;
    pid_t *bees;
    pid_t bear = fork();
    bees = (pid_t *) calloc(bee_count, sizeof(pid_t));
    int **fd;
    fd = (int **) calloc(bee_count, sizeof(int*));
    for(int i = 0; i < bee_count; ++i)
        fd[i] = (int *) calloc(2, sizeof(int));
work:
    for(int i = 0; i < bee_count; ++i)
    {
        pipe(fd[i]);
        bees[i] = fork();
        srand(getpid());
        if(bees[i] == -1)
        {
            printf("Somethin goin wrong with fork()\n");
            fflush(stdout);
            exit(1);
        }
        else if(bees[i] == 0)
        {
            int time = eat_time + 1;
            sleep(rand()%time);
            close(fd[i][0]);
            int temp = honey_portion;
            write(fd[i][1], &temp, sizeof(int));
            exit(0);
        }
    }

    for(int i = 0; i < bee_count; ++i)
    {
        waitpid(bees[i],NULL, 0);
        close(fd[i][1]);
        int tmp = 0;
        read(fd[i][0], &tmp, sizeof(int));
        honey_count += tmp;
        printf("Honey: %d\n", honey_count);
        fflush(stdout);
        if(honey_count >= food_portion)
        {
            honey_count -= food_portion;
            printf("Bear start eating...\n");
            fflush(stdout);
            if(bear == 0)
                sleep(eat_time);
        }
        else
        {
            if(bear == 0)
            {
                sleep(hungry_time);
                if(honey_count < food_portion)
                    exit(0);
            }
            int status;
            waitpid(bear, &status, 0);
            if(status == 0)
            {
                printf("Bear is dead!\n");
                fflush(stdout);
                return 0;
            }
        }
    }
    goto work;
    return 0;
}
	

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 512

int getLenFirst(char *str){
    int len = 0;
    for (int i = 0; i < strlen(str); i++)
		if(str[i] != ' ' && str[i] != '\n' && str[i] != '\t')
            len++;
        else
            break;
    return len;
}

static int cmpstringp(const void *p1, const void *p2){
    int len1 = getLenFirst(*(char * const*)p1);
    int len2 = getLenFirst(*(char * const*)p2);
    return len2 - len1;
}

int wordcount(char *arr)
{
	int count = 1;
	for(int i = 0; i < strlen(arr); ++i)
		if(arr[i] == ' ')
			count++;
	return count;
}

int main()
{
	char **text=NULL;
	int scount = 0;
	FILE *filename;
	filename = fopen("input.txt","r");
	printf("Enter strings count: ");
	scanf("%d", &scount);
	text = (char**) calloc(scount, sizeof(char*));
	printf("Scanning text...\n");
	for(int i = 0; i < scount;i++)
	{
		char buffer[MAX_LEN];
		fgets(buffer, MAX_LEN, filename);
		int len = strlen(buffer);
		text[i] = (char*) calloc(len, sizeof(char));
		strcpy(text[i], buffer);
	}
	fclose(filename);
	qsort(text, scount, sizeof(char*), cmpstringp);
	int count = getLenFirst(text[scount-1]);
	printf("minimal word length is %d\n", count);
	count = wordcount(text[0]);
	printf("word count in frist string is %d\n", count);
	/*for(int i = 0; i < scount; ++i)
		printf("%s", text[i]);*/
	for(int i = 0; i < scount; ++i)
		free(text[i]);
	free(text);
	return 0;
}

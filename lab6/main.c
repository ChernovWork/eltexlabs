#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>

int unit()
{
	srand(getpid());
    printf("Creep %d is going to hunt\n", getpid());
    fflush(stdout);
    sleep(rand()%5);
    int killed_count = rand()%50;
    printf("Creep killed %d peoples\n", killed_count);
    fflush(stdout);
    return killed_count;
}

int main(int argc, char **argv)
{
    int stat;
    if(argc < 2)
    {
        printf("Not enough arguments\n");
        return 0;
    }
    int pcount = atoi(argv[1]);
    while(pcount > 0)
    {
        system("clear");
        printf("Current count of people: %d\n", pcount);
        fflush(stdout);
        pid_t pid = fork();
        if(pid == 0)
            exit(unit());
        sleep(5);
        waitpid(pid, &stat, 0);
        pcount-= WEXITSTATUS(stat);
    }
    return 0;
}

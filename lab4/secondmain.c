#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_LEN 512
#define OUT_FILE "output.txt"

int change_string(char *text, int n)
{
	int count = 0;
	for(int i = 0; i < strlen(text) && count < n; ++i)
	{
		if(text[i] == ' ')
		{
			strncpy(text + i, text, 1);
			++count;
		}
	}
	return count;
}

void print_string(char *text)
{
	FILE *fp = NULL;
	fp = fopen(OUT_FILE, "a+");
	fprintf(fp, "%s", text);
	fclose(fp);
}

void read(FILE *fp, int chcount)//chcount - количество замен
{
	char *buff = NULL;
	char *text = NULL;
	int count = 0;
	for(int i = 0; !feof(fp); ++i)
	{	
		buff = (char*) calloc(MAX_LEN, sizeof(char));
		fgets(buff, MAX_LEN, fp);
		text = (char*) calloc(strlen(buff), sizeof(char));
        strcpy(text, buff);
        free(buff);
        if(count < chcount)
			count += change_string(text, chcount);
        print_string(text);
        free(text);
	}
}

int main(int argc, char *argv[])
{
	 FILE *fp = NULL;
    if (argc < 3)
    {
        fprintf (stderr, "Мало аргументов. Используйте <имя файла> <строка>\n");
        return 0;
    }
    fp=fopen(argv[1], "r");
    if(fp == NULL)
    {
        printf("Не удается открыть файл.\n");
        return 0;
    }
    read(fp, atoi(argv[2]));
    if(fclose(fp))
    {
        printf("Ошибка при закрытии файла.\n");
        return 0;
    }
	return 0;
}

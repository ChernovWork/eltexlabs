#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_LEN 512

void delete_word(char *word, char *text)
{
    char *buff = NULL;
    int wordlen = strlen(word);
   	while((buff = strstr(text, word)))
		strcpy(buff, buff + wordlen);
}

void print_string(char *filename, char *text)
{
	char *buff = strstr(filename, ".");
	buff++;
	strcpy(buff, "rtf");
	FILE *fp = NULL;
	fp = fopen(filename, "a+");
	fprintf(fp, "%s", text);
	fclose(fp);
}

void read_strings(FILE *fp, char *word, char *filename)
{
    char *buff = NULL;
    char *text = NULL;
    for(; !feof(fp);)
    {
		buff = (char*) calloc(MAX_LEN, sizeof(char));
        fgets(buff, MAX_LEN, fp);
        text = (char*) calloc(strlen(buff), sizeof(char));
        strcpy(text, buff);
        free(buff);
        delete_word(word, text);
        print_string(filename, text);
        free(text);
	}
}

int main(int argc, char *argv[])
{
    FILE *fp = NULL;
    if (argc < 3)
    {
        fprintf (stderr, "Мало аргументов. Используйте <имя файла> <строка>\n");
        return 0;
    }
    fp=fopen(argv[1], "r");
    if(fp == NULL)
    {
        printf("Не удается открыть файл.\n");
        return 0;
    }
    read_strings(fp, argv[2], argv[1]);
    if(fclose(fp))
    {
        printf("Ошибка при закрытии файла.\n");
        return 0;
    }
    return 0;
}

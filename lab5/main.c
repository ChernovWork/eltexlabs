#include <stdio.h>
#include <stdlib.h>
#include "func.h"

int main(int argc, char *argv[])
{
	if(argc < 3)
	{
		printf("Not enough arguments");
		return 0;
	}
	int first = atoi(argv[1]);
	int second = atoi(argv[2]);
	printf("Whole: %f\nMod: %d\n", split(first, second), splitmod(first, second));
	return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <unistd.h>
	

#define DLL_PATH "libfuncdyn.so"
#define MAX_LEN 512

int main(int argc, char *argv[])
{
	if(argc < 3)
	{
		printf("Not enough arguments");
		return 0;
	}
	void *library_handler;
	char pwd[MAX_LEN];
	char cwd[MAX_LEN];
	getcwd(pwd, sizeof(pwd));
	snprintf(cwd, sizeof(cwd), "%s/%s", pwd, DLL_PATH);
	library_handler = dlopen(cwd, RTLD_LAZY);
	if(!library_handler)
	{
		printf("dlopen() error: %s\n", dlerror());
		return 0;
	}
	float (*splitfunc) (int a, int b);
	int (*splitmodfunc) (int a, int b);
	splitfunc = dlsym(library_handler, "split");
	splitmodfunc = dlsym(library_handler, "splitmod");
	int first = atoi(argv[1]);
	int second = atoi(argv[2]);
	printf("Whole: %f\nMod: %d\n", (*splitfunc)(first, second), (*splitmodfunc)(first, second));
	dlclose(library_handler);
	return 0;
}

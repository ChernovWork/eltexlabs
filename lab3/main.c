#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 100

struct students
{
	char *fam;
	char *group;
	char *workPlace;
	int score;
};
typedef struct students table;

table* inittable(table *t, int famLen, int groupLen, int workPlaceLen)
{
	t->fam = (char*) calloc(famLen, sizeof(char));
	t->group = (char*) calloc(groupLen, sizeof(char));
	t->workPlace = (char*) calloc(workPlaceLen, sizeof(char));
	return t;
}

table* cpytable(table *t, table *buff)
{	
	strcpy(t->fam, buff->fam);
	strcpy(t->group, buff->group);
	strcpy(t->workPlace, buff->workPlace);
	t->score = buff->score;
	return t;
}

void freetable(table *t)
{
	free(t->fam);
	free(t->group);
	free(t->workPlace);
	free(t);
}

int readtable(table **t)
{
	FILE *in;
	in = fopen("input.txt", "r");
	int i = 0;
	table *buff = NULL;
	buff = (table*) malloc(sizeof(table));
	buff = inittable(buff, MAX_LEN, MAX_LEN, MAX_LEN);
	for(i = 0;!feof(in); ++i)
	{
		fscanf(in, "%s %s %s %d", buff->fam, buff->group, buff->workPlace, &(buff->score));
		t[i] = (table*) malloc(sizeof(table));
		inittable(t[i], strlen(buff->fam), strlen(buff->group), strlen(buff->workPlace));
		cpytable(t[i], buff);
	}
	fclose(in);
	freetable(buff);
	return i;
}

static int cmp(const void *p1, const void *p2)
{
	table *t1 = *(table**)p1;
	table *t2 = *(table**)p2;
	return t2->score - t1->score;
}

void writetable(table *t[], int n)
{
	printf("Фамилия\tГруппа\tМесто прохождения практики\tБалл\n");
	for(int i = 0; i < n; i++)
		printf("%10s\t%6s\t%s\t%26d\n", t[i]->fam, t[i]->group, t[i]->workPlace, t[i]->score);
}

int main()
{
	table **t = NULL;
	t = (table**) calloc(MAX_LEN, sizeof(table*));
	int count = readtable(t);
	qsort(t, count, sizeof(table*), cmp);
	writetable(t, count);
	for(int i = 0; i < count; ++i)
		freetable(t[i]);
	free(t);
	return 0;
}
